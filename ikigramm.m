clc;
clear all;
textpath = 'C:\Users\USER\Desktop\Tezle ilgili her�ey\twetler\';

stopwords_cellstring={'ile', 'de', 've', 'gibi', 'ki', 'kadar','ancak'};

dCell = dir([textpath '*.txt']);
%list=dir('*.txt');
matris = cell(650,200);
for i=1:650
    for j = 1:200
    matris{i,j} = '';
    end
    matris{i,200} = int2str(0);
end
satir = 1;

for i=1:length(dCell);
    oku=fopen([textpath dCell(i).name]);
    CC = textscan(oku,'%s','delimiter','\n');
    fclose(oku);
    CC = CC{1};
    [cx cy] = size(CC);
    for m = 1:cx
        C = textscan(char(CC{m,1}),'%s');
        [rcx rcy] = size(C{1});
            for n = 1:rcx
                C{1}{n,1} = regexprep(char(C{1}{n,1}),'[.,''":*?!;]','');
            end
        d = C{1};
        d=d';
        out_str1 = strjoin(d(~ismember(d,stopwords_cellstring)),' ');
        kelime = regexp(out_str1,'\s','Split');
        NW = numel(kelime); %// number of words
        char_arr1 = char(kelime'); %//' convert split cells into a char array
        ind1 = bsxfun(@plus,[1:NW*2]',[0:size(char_arr1,2)-2]*NW); %//' get indices
        %// to be used for indexing into char array
        t1 = reshape(char_arr1(ind1),NW,2,[]);
        t2 = reshape(permute(t1,[2 1 3]),2,[])'; %//' char array with rows for each pair
        out = reshape(mat2cell(t2,ones(1,size(t2,1)),2),NW,[])'; %//'
        out(reshape(any(t2==' ',2),NW,[])')={''}; %//' Use only paired-elements cells
        out = [kelime ; out]; %// output
        index = 1;
        [x y] = size(out);
        for j = 1:y
            for k = 2:x
                if(strcmp('',char(out{k,j}))==0)
                state = 0;
                    for l = 1:index
                        if(strcmp(cellstr(matris(satir,l)),lower(char(out{k,j})))==1)
                        state = 1;
                        end
                    end
                    if (state == 0)
                        matris{satir,index} = lower(char(out{k,j}));
                        index = index + 1;
                    end
                end
            end
        end
        satir = satir + 1;
        clear out;
    end
end
for j = 1:200
    matris{1,j} = int2str(j);
end
matris{1,200} = 'class';
ds = cell2dataset(matris);
export(ds,'file','notr2.csv','delimiter',',');

